FROM python

RUN pip3 install slackclient

COPY src /src

CMD python3 src/main.py

