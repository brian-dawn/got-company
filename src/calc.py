from collections import deque
import re
import operator

def isfloat(x):
    try:
        a = float(x)
    except ValueError:
        return False
    else:
        return True

def evaluate_math(input):
    try:
        token_regex = r"(\b\d*[\.]?\d+\b|[\(\)\+\*\-\/]|\b[a-zA-Z]*\b|)"
        tokens = re.findall(token_regex, input)

        #print(tokens)

        # known_functions = {
        #    "sin": math.sin,
        #    "cos": math.cos,
        #    "tan": math.tan] # todo

        parens = ["(", ")"]

        LEFT = 1
        RIGHT = 2
        operators = {
            "*": {"precedence":2, "associativity": LEFT, "fn": operator.mul},
            "x": {"precedence":2, "associativity": LEFT, "fn": operator.mul},
            "/": {"precedence":2, "associativity": LEFT, "fn": operator.truediv},
            "+": {"precedence":1, "associativity": LEFT, "fn": operator.add},
            "-": {"precedence":1, "associativity": LEFT, "fn": operator.sub}
        }

        op_stack = []
        rpn = deque([])

        # parse
        for token in tokens:
            if isfloat(token):
                rpn.append(token)
            # if token in known_functions.keys():
            #     op_stack.append(token) todo
            if token in operators.keys():
                precedence = operators[token]["precedence"]
                while (len(op_stack) != 0 and
                        (op_stack[-1] not in parens) and
                        ((operators[op_stack[-1]]["precedence"] > precedence) or
                        (operators[op_stack[-1]]["precedence"] == precedence and operators[op_stack[-1]]["associativity"] == LEFT))):
                    rpn.append(op_stack.pop())
                op_stack.append(token)
            if token == "(":
                op_stack.append(token)
            if token == ")":
                while (op_stack[-1] != "("):
                    rpn.append(op_stack.pop())
                    if len(op_stack) == 0:
                        return "mismatched parens"
                if op_stack[-1] == "(":
                    op_stack.pop()
        while len(op_stack) != 0:
            if op_stack[-1] in parens:
                return "mismatched parens"
            rpn.append(op_stack.pop())

        #print("rpn=",rpn)

        #eval
        stack = []
        for token in rpn:
            if token in operators.keys():
                operand_2 = stack.pop()
                operand_1 = stack.pop()
                result = operators[token]["fn"](float(operand_1), float(operand_2)) # should these get coerced earlier?
                stack.append(result)
            else:
                stack.append(token)
        return stack.pop()
    except:
        return "I can't do that math yet"