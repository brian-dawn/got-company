import os
import slack
from calc import evaluate_math

@slack.RTMClient.run_on(event='message')
def say_hello(**payload):
    try:
        data = payload['data']
        web_client = payload['web_client']
        rtm_client = payload['rtm_client']
        channel_id = data['channel']
        thread_ts = data['ts']
        user = data['user']

        if 'text' in data and  'Hello' in data['text']:
            print("hello!")
            web_client.chat_postMessage(
                channel=channel_id,
                text=f"Hello there <@{user}>!",
                thread_ts=thread_ts
            )
        if 'text' in data and 'math' in data['text']:
            print("doing some math...")
            result = evaluate_math(data['text'].replace("math",""))
            web_client.chat_postMessage(
                channel=channel_id,
                text=f"```{result}```"
            )
    except:
        pass

print('starting slack bot')
if len(os.environ["SLACK_API_TOKEN"]) == 0:
    print('failed to find the api token')
slack_token = os.environ["SLACK_API_TOKEN"]
rtm_client = slack.RTMClient(token=slack_token)
rtm_client.start()
